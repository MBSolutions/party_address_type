# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Party Address Type',
    'name_de_DE': 'Parteien Adressen Typen',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds support of address types for parties
    ''',
    'description_de_DE': '''
    - Fügt Unterstützung von Adresstypen für Parteien hinzu.
    ''',
    'depends': [
        'party',
    ],
    'xml': [
        'party.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
